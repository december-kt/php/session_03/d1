<?php 

	// Objects as Variables

	$buildingObj = (object) [
		'name' => 'Caswynn Building',
		'floors' => 8,
		'address' => (object) [
			'barangay' => 'Sacred Heart',
			'city' => 'Quezon City',
			'country' => 'Philippines'
		]
	];

	// Objects from Classes

	class Building {
		// Properties
		// Access Modifiers -- public, private
		protected $name;
		public $floors;
		public $address;

		// Constructor
		public function __construct($name, $floors, $address) {
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}

		// Methods
		public function printName() {
			return "The name of the building is $this->name";
		}
	}

	// Instances

	$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

	// Inheritance and Polymorphism

	// Inherit -- extend keyword
	class Condominium extends Building {
		public function printName() {
			return "The name of the condominium is $this->name";
		}

		// Encapsulation
		// getters(read-only) and setters(write-only)
		public function getName() {
			return $this->name;
		}

		public function setName($name) {
			$this->name = $name;
		}
	}

	// Instance
	$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines')
?>